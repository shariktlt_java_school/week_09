package ru.edu.task3.xml;

/**
 * ReadOnly
 */
public class PromDependency implements DependencyObject {
    @Override
    public String getValue() {
        return "PROM";
    }
}
