package ru.edu.task4.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
public class AppJava {

    public static MainContainer run(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }
}
